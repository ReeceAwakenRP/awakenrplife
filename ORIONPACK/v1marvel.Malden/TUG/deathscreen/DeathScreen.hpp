class DeathScreen {
    idd = 7300;
    name = "Life_Death_Screen";
    movingEnable = false;
    enableSimulation = true;
    onLoad = "uiNamespace setVariable ['Life_Death_Screen', _this select 0]";
    onUnload = "uiNamespace setVariable ['Life_Death_Screen', objNull]";
    onDestroy = "uiNamespace setVariable ['Life_Death_Screen', objNull]";
    class controlsBackground {
    };
    class controls {
		class Sts_Rsc_DeathProgress: Life_RscProgress
		{
			idc = 99532;
			x = 0.396875 * safezoneW + safezoneX;
			y = 0.643 * safezoneH + safezoneY;
			w = 0.20625 * safezoneW;
			h = 0.044 * safezoneH;
    		colorFrame[] = {0, 0, 0, 1};
    		colorBackground[] = {0,0,0,0.7};
    		colorBar[] = {1,0,0,1};
		};
		class Sts_Rsc_DeathProgressText: Life_RscStructuredText
		{
			align = "center";
			idc = 99533;
			text = "";
			x = 0.396875 * safezoneW + safezoneX;
			y = 0.643 * safezoneH + safezoneY;
			w = 0.20625 * safezoneW;
			h = 0.044 * safezoneH;
			class Attributes
    		{
        		font = "PuristaLight";
        		color = "#E5E5E5";
        		align = "center";
        		shadow = "false";
				valign = "Middle";
    		};
		};
		class Sts_RscRespawnBtn: Life_RscButtonMenu
		{
			idc = 7302;
			text = "Respawn"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.033 * safezoneH;
			onbuttonClick = "SOMEVAR = true;life_is_alive = true;closeDialog 0; life_respawned = true; [] call life_fnc_spawnMenu;";
			class Attributes
    		{
        		font = "PuristaLight";
        		color = "#E5E5E5";
        		align = "center";
        		shadow = "false";
				valign = "Middle";
    		};
		};
		class Sts_RscRequestBtn: Life_RscButtonMenu
		{
			idc = 7303;
			text = "Request"; //--- ToDo: Localize;
			x = 0.427812 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0721875 * safezoneW;
			h = 0.033 * safezoneH;
			onbuttonClick = "[] call Sts_MedicRequest";
		};
		class Sts_RscNearestMedic: Life_RscStructuredText
		{
			idc = 955325;
			text = ""; //--- ToDo: Localize;
			x = 0.398937 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.201094 * safezoneW;
			h = 0.099 * safezoneH;
			colorBackground[] = {0,0,0,0.7};
		};
	};
};

