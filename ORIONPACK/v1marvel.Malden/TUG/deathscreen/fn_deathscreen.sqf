params ["_unit"];
diag_LOG "ENTERED";
SOMEVAR = false;
Sts_MedicRequested = false;
Sts_DeathscreenObject = _unit;
life_respawned = false;
diag_log "Lauching Deathscreen Function";
Sts_MedicRequest = {
    _medicsOnline = {_x != player && {side _x isEqualTo independent} && {alive _x}} count playableUnits > 0;
    Sts_DeathscreenObject setVariable ["Revive",false,true];
    if (_medicsOnline) then {
        [Sts_DeathscreenObject,profileName] remoteExecCall ["life_fnc_medicRequest",independent];
    } else {
        [Sts_DeathscreenObject,profileName] remoteExecCall ["life_fnc_medicRequest",west];
    };
    Sts_MedicRequested = true;
};

(uiNamespace getVariable ["Life_Death_Screen",displayNull]) displaySetEventHandler ["KeyDown","if ((_this select 1) isEqualTo 1) then {true}"]; //Block the ESC menu

    disableSerialization;
    _display = uiNamespace getVariable ["Life_Death_Screen",displayNull];

    if(isNull _display)then{createDialog "Deathscreen"};

    _Progress = _display displayCtrl 99532;
    _ProgressText = _display displayCtrl 99533;
    _Progress progressSetPosition 1;
    _ProgressText ctrlSetText "Bleeding Out";
    _BtnRespawn =   _display displayCtrl 7302;
    _BtnRequest = _display displayCtrl 7303;
    _NearestMedicControl = _display displayCtrl 955325;
    _BtnRequest ctrlEnable false;
    _BtnRespawn ctrlEnable false;
    _ForceRespawnTimer = 0;
    _RespawnTimer = 0;
    _RequestTimer = 0;
    life_is_alive = false;

    while{true}do{
        if(life_is_alive || life_respawned || SOMEVAR)exitWith{
            systemChat "Welcome Back To Life";
            Sts_DeathscreenObject = objNull;
        };
        //systemChat format ["%1--%2--%3",_ForceRespawnTimer,_RequestTimer,_RespawnTimer];
        _ForceRespawnCounter = getNumber(missionConfigFile >> "DeathscreenSettings" >> "ForceRespawn");
        _ProgressPerPercent = (1/_ForceRespawnCounter);
        _currentPercent = 1-(_ForceRespawnTimer * _ProgressPerPercent);
        _RespawnTimerCounter = getNumber(missionConfigFile >> "DeathscreenSettings" >> "RespawnTimer");
        _timeBetweenRequestCounter = getNumber(missionConfigFile >> "DeathscreenSettings" >> "timeBetweenRequest");
        if(_ForceRespawnTimer >= _ForceRespawnCounter)exitWith{
            diag_log format ["%1",buttonAction _BtnRespawn];
            _BtnRespawn ctrlEnable true;
            _function = gettext(missionconfigfile >> "DeathScreen" >> "controls" >> "Sts_RscRespawnBtn" >> "onbuttonClick");
            if(life_is_alive || life_respawned || SOMEVAR)exitWith{
                systemChat "Welcome Back To Life";
                Sts_DeathscreenObject = objNull;
            };
            call (compile _function);
            closeDialog 0;
            diag_log "Exiting Deathscreen Function";
            closeDialog 0;
            life_respawned = true;
            sleep 1;
            [] call life_fnc_spawnMenu;
            Sts_DeathscreenObject = objNull;
        };
        if(_RespawnTimer >= _RespawnTimerCounter)then{_BtnRespawn ctrlEnable true};
        if(_RequestTimer >= _timeBetweenRequestCounter)then{
            _BtnRequest ctrlEnable true;
        };
        if(Sts_MedicRequested)then{
            _RequestTimer = 0;
            Sts_MedicRequested = false;
            _BtnRequest ctrlEnable false;
        };
        _ForceRespawnTimer = _ForceRespawnTimer + 1;
        _RespawnTimer = _RespawnTimer + 1;
        _RequestTimer = _RequestTimer + 1;
        //Medics Online Section.
        _medics = [];
        {
            _medicsOnline = {_x != player && {side _x isEqualTo independent} && {alive _x}} count playableUnits > 0;
            if(_medicsOnline)then{
                {
                    if(_x != player && side _x isEqualTo independent && alive _x)then{
                        _medics pushBack _x;
                    };
                }forEach playableUnits;
            }else{
                {
                    if(_x != player && side _x isEqualTo west && alive _x)then{
                        _medics pushBack _x;
                    };
                }forEach playableUnits;
            };
        }forEach playableUnits;
        _NearestMedic = objNull;
        _NearestMedicDistance = 9999999999;
        {
            _distance = Sts_DeathscreenObject distance _x;
            if(isNull _NearestMedic)then{
                _NearestMedic = _x;
                _NearestMedicDistance = _distance;
            }else{
                if(_distance < _NearestMedicDistance)then{
                    _NearestMedic = _x;
                    _NearestMedicDistance = _distance;
                };
            };
        }forEach _medics;
        _Progress progressSetPosition _currentPercent;
        _ForceRespawnCounter = getNumber(missionConfigFile >> "DeathscreenSettings" >> "ForceRespawn");
        _text = [(_ForceRespawnCounter - _ForceRespawnTimer),"MM:SS"] call BIS_fnc_secondsToString;
        _textFormat = format ["<t align='center'>Bleeding out : %1</t>",_text];
        _ProgressText ctrlSetStructuredText parseText format["%1",_textFormat];
        _NearMedictext = "No Medics Nearby";
        if(isNull _NearestMedic)then{
            _NearMedictext = "<t align='center' size='1.5px'>No Medics online</t>";
        }else{
            _MedicName = name _NearestMedic;
            _MedicDistance = Sts_DeathscreenObject distance _NearestMedic;
            _MedicSpeed = speed _NearestMedic;
            _NearMedictext = format ["<t align='center'>Nearest Medic</t><br/><t align='Left'>Name</t><t align='Right'>%1</t><br/><t align='Left'>Distance</t><t align='Right'>%2</t><br/><t align='Left'>Speed</t><t align='Right'>%3</t><br/>",_MedicName,_MedicDistance,_MedicSpeed];
        };
        _NearestMedicControl ctrlSetStructuredText parseText format["%1",_NearMedictext];
        sleep 1;
        if(life_is_alive || life_respawned || SOMEVAR)exitWith{
            systemChat "Welcome Back To Life";
            Sts_DeathscreenObject = objNull;
        };
    };

    if(true)exitWith{
        diag_log "Exiting Deathscreen Function";
    };
    diag_log "EXITED COMPLETLY";



